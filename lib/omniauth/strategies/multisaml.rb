require 'omniauth'
require 'omniauth-saml'
require 'ruby-saml'

module OmniAuth
  module Strategies
    class Multisaml < SAML
      option :idp_names

      def callback_phase
        saml_response = request.params["SAMLResponse"]
        log(:info, "SAMLResponse: `#{saml_response}`")
        super
      end

      def idp_names
        options[:idp_names].respond_to?(:call) && options[:idp_names].call(env)
      end

      private

      def on_subpath?(subpath)
        idp_names.any? { |idp_name|
          on_path?("#{path_prefix}/#{name}/#{idp_name}/#{subpath}")
        }
      end
    end
  end
end